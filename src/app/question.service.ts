import { Injectable } from "@angular/core";

import { QuestionBase } from './question-base';
import { DropdownQuestion } from './question-dropdown';
import { TextboxQuestion } from './question-textbox';
import { of } from 'rxjs';

@Injectable()

export class QuestionService {

    getQuestions() {
        const questions: QuestionBase<string>[] = [
            new DropdownQuestion({
                key: 'country',
                label: 'Country and City',
                options: [
                    {key: 'england',  value: 'England'},
                    {key: 'londan',  value: 'Londan'},
                    {key: 'brighton',  value: 'Brighton'},
                    {key: 'liverpool', value: 'liverpool'}
                ],
                order: 4
            }),

            new TextboxQuestion({
                key: 'firstName',
                label: 'First name',
                value: 'saeed',
                required: true,
                order: 1
            }),

            new TextboxQuestion({
                key: 'lastName',
                label: 'Last name',
                value: 'moradi',
                required: true,
                order: 2
            }),

            new TextboxQuestion({
                key: 'emailAddress',
                label: 'Email',
                type: 'email',
                order: 3
            }),
        ];
        return of(questions.sort((a,b) => a.order - b.order));
    }

}